package ru.krista.summer2023;

public interface Node {
    String getName();

    void addChild(Node child);

    Node find(Node childNode);

    boolean removeChild();

    boolean removeAllChild();

    String renameChild(int id);

}
