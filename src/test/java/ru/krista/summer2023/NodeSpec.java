package ru.krista.summer2023;


import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class NodeSpec {

    @Test
    @DisplayName("Вы создали главный узел")
    void createNode(){
//        UUID uuid = UUID.randomUUID();
        Node node = new NodeImpl("mainNode", UUID.randomUUID());
        assertEquals("mainNode", node.getName(), "ошибка, имя не корректно");
    }


    @Test
    @DisplayName("Вы создали дочерний узел")
    void createChildNode(){
        Node node = new NodeImpl("mainNode", UUID.randomUUID());
        Node child = new NodeImpl("childNode", UUID.randomUUID());
        node.addChild(child);
        assertEquals("childNode", child.getName(), "ошибка, имя дочерней ветки некорректно");
    }


    @Test
    @DisplayName("Вы нашли дочерний узел")
    void findChild(){
        Node node = new NodeImpl("mainNode", UUID.randomUUID());
        Node child = new NodeImpl("childNode", UUID.randomUUID());
        node.addChild(child);
        assertEquals("childNode", node.find(child).getName(), "ошибка, имя не корректно");
    }


    @Test
    @DisplayName("Вы удалили дочерний узел")
    void removeChild(){
        Node node = new NodeImpl("mainNode", UUID.randomUUID());
        Node child = new NodeImpl("childNode", UUID.randomUUID());
        node.addChild(child);
        assertTrue(node.removeChild(), "ошибка, узел не удален");
    }


    @Test
    @DisplayName("Вы удалили все узлы")
    void removeAllChilds(){
        Node node = new NodeImpl("mainNode", UUID.randomUUID());
        Node child = new NodeImpl("childNode", UUID.randomUUID());
        Node child2 = new NodeImpl("childNode", UUID.randomUUID());
        node.addChild(child);
        node.addChild(child2);
        assertTrue(node.removeAllChild(), "ошибка, остались узлы");
    }


    @Test
    @DisplayName("Вы переименовали узел")
    void renameChild(){
        Node node = new NodeImpl("mainNode", UUID.randomUUID());
        Node child = new NodeImpl("childNode", UUID.randomUUID());
        node.addChild(child);

        assertEquals("childNode", node.renameChild(1), "ошибка, имя не корректно");
    }
}
